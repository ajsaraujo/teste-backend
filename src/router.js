const path = require('path');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const UserController = require('./controllers/UserController');
const TokenController = require('./controllers/TokenController');
const MovieController = require('./controllers/MovieController');
const ReviewController = require('./controllers/ReviewController');
const ArtistController = require('./controllers/ArtistController');

const User = require('./models/User');
const Movie = require('./models/Movie');
const RevokedToken = require('./models/RevokedToken');
const Review = require('./models/Review');
const Artist = require('./models/Artist');

const validate = require('./middlewares/validate');
const authenticate = require('./middlewares/authenticate');
const authorize = require('./middlewares/authorize');
const limitRequests = require('./middlewares/limitRequests');

const PasswordUtils = require('./utils/PasswordUtils');
const DirectoryUtils = require('./utils/DirectoryUtils');
const TokenUtils = require('./utils/TokenUtils');

const tokenUtils = new TokenUtils(jwt);

function assembleControllers() {
    const passwordUtils = new PasswordUtils(bcrypt);

    return {
        user: new UserController(User, passwordUtils, tokenUtils),
        token: new TokenController(RevokedToken, tokenUtils),
        movie: new MovieController(Movie, Review),
        review: new ReviewController(Review, Movie, User),
        artist: new ArtistController(Artist)
    };
}

function assembleMiddlewares() {
    return {
        validate,
        authenticate: authenticate(tokenUtils),
        authorize,
        limitRequests
    };
}

async function injectRoutes(router, controllers, middlewares) {
    const routesDirectory = path.join(__dirname, 'routes/');
    const routes = await DirectoryUtils.getFilesInDirectory(routesDirectory, 'Route.js');

    routes.forEach(addRouteFunction => {
        addRouteFunction(router, controllers, middlewares);
    });
}

async function createRouter(router) {
    router.get('/', (req, res) => res.status(200).json({ message: 'Welcome to Ioasys Movie Database!' }));

    const controllers = assembleControllers();
    const middlewares = assembleMiddlewares();

    injectRoutes(router, controllers, middlewares);

    return router;
}

module.exports = createRouter;
