const rateLimit = require('express-rate-limit');

const MINUTE = 60 * 1000;

// 100 requests a cada 15 minutos
const slightly = rateLimit({
    windowMs: 15 * MINUTE,
    max: 100
});

// 50 requests a cada 15 minutos
const regularly = rateLimit({
    windowMs: 15 * MINUTE,
    max: 50
});

// 5 requests a cada 15 minutos
const heavily = rateLimit({
    windowMs: 15 * MINUTE,
    max: 5
});

module.exports = { slightly, heavily, regularly };
