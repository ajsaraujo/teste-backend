function authenticate(tokenUtils) {
    return (req, res, next) => {
        const token = req.headers['x-access-token'];

        if (tokenUtils.isMalformed(token)) {
            return res.status(400).json({ message: 'Malformed or missing token. You should provide a token with the following format: Bearer <token>' });
        }

        const { isValid, payload } = tokenUtils.verify(token, process.env.ACCESS_SECRET);

        if (!isValid) {
            return res.status(401).json({ message: 'Invalid token. Refresh your token or authenticate again.' });
        }

        req.user = payload;

        return next();
    };
}

module.exports = authenticate;
