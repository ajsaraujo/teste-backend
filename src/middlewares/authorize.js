function authorize(...roles) {
    return (req, res, next) => {
        if (!req.user || !req.user.role) {
            return next(new Error('Tried to authorize without authentication'));
        }

        if (!roles.includes(req.user.role)) {
            return res.status(403).json({ message: `${req.user.role || 'User'} does not have access to this resource.` });
        }

        return next();
    };
}

module.exports = authorize;
