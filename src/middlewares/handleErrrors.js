function handleErrors(err, res) {
    console.log(err.message);
    return res.status(500).json({ message: err.message });
}

module.exports = handleErrors;
