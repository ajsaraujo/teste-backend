class ReviewController {
    constructor(Review, Movie, User) {
        this.Review = Review;
        this.Movie = Movie;
        this.User = User;
    }

    async create(req, res, next) {
        try {
            const user = await this.User.findOneActive({ _id: req.user.id });

            if (!user) {
                return res.status(404).json({ message: 'User not found.' });
            }

            const movie = await this.Movie.findById(req.body.movieId);

            if (!movie) {
                return res.status(404).json({ message: 'Movie not found.' });
            }

            req.body.userId = req.user.id;

            const review = await this.Review.create(req.body);

            return res.status(201).json(review);
        } catch (err) {
            return next(err);
        }
    }
}

module.exports = ReviewController;
