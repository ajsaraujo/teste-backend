class TokenController {
    constructor(RevokedToken, tokenUtils) {
        this.RevokedToken = RevokedToken;
        this.tokenUtils = tokenUtils;
    }

    async refresh(req, res, next) {
        try {
            const token = req.headers['x-refresh-token'];

            if (this.tokenUtils.isMalformed(token)) {
                return res.status(400).json({ message: 'Malformed token. You should provide a token with the following format: Bearer <token>.' });
            }

            const { isValid, payload } = this.tokenUtils.verify(token, process.env.REFRESH_SECRET);

            const tokenWasRevoked = await this.isRevoked(token);

            if (!isValid || tokenWasRevoked) {
                return res.status(401).json({ message: 'Invalid token. Please authenticate again.' });
            }

            delete payload.iat;
            delete payload.exp;

            const accessToken = this.tokenUtils.createAccessToken(payload);

            return res.status(200).json({ 'x-access-token': accessToken });
        } catch (err) {
            return next(err);
        }
    }

    async revoke(req, res, next) {
        try {
            const token = req.headers['x-refresh-token'];

            if (this.tokenUtils.isMalformed(token)) {
                return res.status(400).json({ message: 'Malformed token. You should provide a token with the following format: Bearer <token>.' });
            }

            const { isValid } = this.tokenUtils.verify(token, process.env.REFRESH_SECRET);

            const isRevoked = await this.isRevoked(token);

            if (!isValid || isRevoked) {
                return res.status(200).json({ message: 'Token revoked successfully.' });
            }

            await this.RevokedToken.create({ value: token.split(' ')[1] });

            return res.status(200).json({ message: 'Token revoked successfully.' });
        } catch (err) {
            return next(err);
        }
    }

    async isRevoked(token) {
        const value = token.split(' ')[1];

        const revokedToken = await this.RevokedToken.findOne({ value });

        return Boolean(revokedToken);
    }
}

module.exports = TokenController;
