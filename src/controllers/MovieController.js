class MovieController {
    constructor(Movie, Review) {
        this.Movie = Movie;
        this.Review = Review;
    }

    async create(req, res, next) {
        try {
            const movie = await this.Movie.create(req.body);

            return res.status(201).json(movie);
        } catch (err) {
            return next(err);
        }
    }

    async get(req, res, next) {
        try {
            const movie = await this.Movie.findById(req.params.id);

            if (!movie) {
                return res.status(404).json({ message: 'Movie not found.' });
            }

            // Eu usei filter pq o where não permitira eu castar o ObjectId
            // para string.
            const allReviews = await this.Review.find();
            const reviews = allReviews.filter(
                review => review.movieId.toString() === req.params.id
            );

            // Idealmente esse cálculo estaria sendo delegado ao BD,
            // mas por ser mais fácil acabei fazendo programaticamente.
            const ratingSum = reviews
                .map(review => review.rating)
                .reduce((sum, current) => sum + current);

            const average = ratingSum / reviews.length;

            const plainObject = movie.toObject();
            plainObject.averageRating = average;
            plainObject.reviews = reviews;

            return res.status(200).json(plainObject);
        } catch (err) {
            next(err);
        }
    }
}

module.exports = MovieController;
