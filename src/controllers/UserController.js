class UserController {
    constructor(User, passwordMatcher, tokenUtils) {
        this.User = User;
        this.passwordMatcher = passwordMatcher;
        this.tokenUtils = tokenUtils;
    }

    async register(req, res, next) {
        const { email } = req.body;

        try {
            const emailIsTaken = await this.User.findOne({ email });

            if (emailIsTaken) {
                return res.status(400).json({ message: `Email ${email} is already in use.` });
            }

            const user = await this.User.create(req.body);

            user.password = undefined;
            user.isActive = undefined;

            return res.status(201).json(user);
        } catch (error) {
            return next(error);
        }
    }

    async auth(req, res, next) {
        const { email, password } = req.body;

        const user = await this.User.findOneActive({ email });

        const realPassword = user && user.password;

        const badCredentials = !user || !user.isActive
            || !(await this.passwordMatcher.match(password, realPassword));

        if (badCredentials) {
            return res.status(400).json({ message: 'Invalid email and/or password.' });
        }

        user.password = undefined;
        user.isActive = undefined;

        // JWT não deixa a gente passar o objeto que o mongoose retorna.
        const payload = {
            id: user.id, name: user.name, email: user.email, role: user.role
        };

        const accessToken = this.tokenUtils.createAccessToken(payload);
        const refreshToken = this.tokenUtils.createRefreshToken(payload);

        return res.status(200).json({ user, 'x-access-token': accessToken, 'x-refresh-token': refreshToken });
    }

    async update(req, res, next) {
        try {
            const user = await this.User.findOneActive({ _id: req.user.id });

            if (!user) {
                return res.status(404).json({ message: 'User not found.' });
            }

            const userWantsToChangeEmail = req.user.email !== req.body.email;

            if (userWantsToChangeEmail) {
                const emailIsTaken = await this.User.findOneActive({ email: req.body.email });

                if (emailIsTaken) {
                    return res.status(400).json({ message: `Email ${req.body.email} is already taken.` });
                }
            }

            Object.keys(req.body).forEach(key => {
                user[key] = req.body[key];
            });

            await user.save();

            user.password = undefined;
            user.isActive = undefined;

            return res.status(200).json(user);
        } catch (err) {
            return next(err);
        }
    }

    async delete(req, res, next) {
        try {
            const user = await this.User.findOneActive({ _id: req.user.id });

            if (!user) {
                return res.status(404).json({ message: 'User not found.' });
            }

            user.isActive = false;

            await this.User.findByIdAndUpdate(req.user.id, user);

            return res.status(200).json({ message: 'Account deleted.' });
        } catch (err) {
            return next(err);
        }
    }

    async promote(req, res, next) {
        try {
            const { id } = req.params;

            if (!id) {
                return res.status(400).json({ message: 'You should provide an user id at the request parameters.' });
            }

            const user = await this.User.findOneActive({ _id: id });

            if (!user) {
                return res.status(404).json({ message: 'User not found.' });
            }

            user.role = 'Admin';
            await user.save();

            return res.status(200).json({ message: 'User was promoted.' });
        } catch (err) {
            return next(err);
        }
    }
}

module.exports = UserController;
