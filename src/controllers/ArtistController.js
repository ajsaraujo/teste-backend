class ArtistController {
    constructor(Artist) {
        this.Artist = Artist;
    }

    async create(req, res, next) {
        try {
            const artist = await this.Artist.create(req.body);

            return res.status(201).json(artist);
        } catch (err) {
            return next(err);
        }
    }

    createWork() {

    }
}

module.exports = ArtistController;
