class Database {
    constructor(mongoose) {
        this.mongoose = mongoose;
    }

    async connect() {
        const uri = process.env.DB_URI;

        const options = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        };

        await this.mongoose.connect(uri, options);

        console.log('Successfully connected to the database!');
    }

    async close() {
        await this.mongoose.connection.close();
    }
}

module.exports = Database;
