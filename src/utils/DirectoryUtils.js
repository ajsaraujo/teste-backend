const path = require('path');
const { readdir } = require('fs');
const { promisify } = require('util');

async function getFilesInDirectory(directory, fileNameEnding) {
    const read = promisify(readdir);
    const directoryContent = await read(directory);

    /* eslint-disable */
    return directoryContent
        .filter(file => file.endsWith(fileNameEnding))
        .map(file => {
            const filePath = path.join(directory, file);
            return require(filePath);
        });
    /* eslint-enable */
}

module.exports = { getFilesInDirectory };
