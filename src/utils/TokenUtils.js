class TokenUtils {
    constructor(jwt) {
        this.jwt = jwt;
    }

    createAccessToken(payload) {
        return `Bearer ${this.jwt.sign(
            payload,
            process.env.ACCESS_SECRET,
            { expiresIn: process.env.ACCESS_LIFE_SPAN }
        )}`;
    }

    createRefreshToken(payload) {
        return `Bearer ${this.jwt.sign(
            payload,
            process.env.REFRESH_SECRET,
            { expiresIn: process.env.REFRESH_LIFE_SPAN }
        )}`;
    }

    verify(token, secret) {
        try {
            const value = token.split(' ')[1];

            const payload = this.jwt.verify(value, secret);

            return { isValid: true, payload };
        } catch (error) {
            return { isValid: false };
        }
    }

    // TODO: Refatorar para método estático!
    /* eslint-disable class-methods-use-this */
    isMalformed(token) {
        if (!token) {
            return true;
        }

        const [type, value] = token.split(' ');

        return !type || !value || type !== 'Bearer';
    }
    /* eslint-disable class-methods-use-this */
}

module.exports = TokenUtils;
