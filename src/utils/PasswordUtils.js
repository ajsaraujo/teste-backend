class PasswordUtils {
    constructor(crypto) {
        this.crypto = crypto;
    }

    async encrypt(data) {
        const encryptedData = await this.crypto.hash(data, 10);
        return encryptedData;
    }

    async match(plainText, encryptedText) {
        if (!plainText || !encryptedText) {
            return false;
        }

        return this.crypto.compare(plainText, encryptedText);
    }
}

module.exports = PasswordUtils;
