function injectTokenRoute(router, controllers) {
    const { token } = controllers;

    router.post('/refresh', async (req, res, next) => {
        await token.refresh(req, res, next);
    });

    router.post('/revoke', async (req, res, next) => {
        await token.revoke(req, res, next);
    });
}

module.exports = injectTokenRoute;
