const Artist = require('../models/Artist');

function injectArtistRoute(router, controllers, middlewares) {
    const { artist } = controllers;
    const { authenticate, authorize, validate } = middlewares;

    router.post('/artist',
        authenticate,
        authorize('Admin'),
        validate(Artist.joiValidator),

        async (req, res, next) => {
            await artist.create(req, res, next);
        });
}

module.exports = injectArtistRoute;
