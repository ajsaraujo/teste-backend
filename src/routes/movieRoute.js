const Movie = require('../models/Movie');

function injectMovieRoute(router, controllers, middlewares) {
    const { movie } = controllers;
    const { validate, authenticate, authorize } = middlewares;

    router.post('/movie',
        authenticate,
        authorize('Admin'),
        validate(Movie.joiValidator),

        async (req, res, next) => {
            await movie.create(req, res, next);
        });

    router.get('/movie/:id', async (req, res, next) => {
        await movie.get(req, res, next);
    });
}

module.exports = injectMovieRoute;
