const Review = require('../models/Review');

function injectReviewRoute(router, controllers, middlewares) {
    const { review } = controllers;
    const { authenticate, authorize, validate } = middlewares;

    router.post('/review',
        authenticate,
        authorize('User'),
        validate(Review.joiValidator),

        async (req, res, next) => {
            await review.create(req, res, next);
        });
}

module.exports = injectReviewRoute;

