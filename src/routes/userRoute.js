const User = require('../models/User');

function injectUserRoute(router, controllers, middlewares) {
    const { user } = controllers;
    const {
        validate, authenticate, authorize, limitRequests
    } = middlewares;

    router.post('/auth',
        limitRequests.regularly,
        validate(User.authValidator),

        async (req, res, next) => {
            await user.auth(req, res, next);
        });

    router.post('/register',
        limitRequests.heavily,
        validate(User.joiValidator),

        async (req, res, next) => {
            await user.register(req, res, next);
        });

    router.put('/user',
        authenticate,
        validate(User.joiValidator),

        async (req, res, next) => {
            await user.update(req, res, next);
        });

    router.delete('/user',
        limitRequests.heavily,
        authenticate,

        async (req, res, next) => {
            await user.delete(req, res, next);
        });

    router.post('/user/promote/:id',
        limitRequests.heavily,
        authenticate,
        authorize('Admin'),

        async (req, res, next) => {
            await user.promote(req, res, next);
        });
}

module.exports = injectUserRoute;
