const Database = require('./database');
const handleErrors = require('./middlewares/handleErrrors');

function createApp(express, router, middlewares, mongoose) {
    const app = express;

    middlewares.forEach(middleware => {
        app.use(middleware);
    });

    app.use('/api', router);

    app.database = new Database(mongoose);

    return app;
}

module.exports = createApp;
