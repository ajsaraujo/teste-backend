const { Schema, model } = require('mongoose');
const Joi = require('joi');

const MovieSchema = new Schema({
    title: {
        type: String,
        required: true
    },

    year: {
        type: Number,
        required: true
    },

    synopsis: {
        type: String,
        required: false,
    },

    length: {
        type: Number,
        required: true
    }
});

const Movie = model('Movie', MovieSchema);

Movie.joiValidator = Joi.object({
    title: Joi.string().required().max(250),
    year: Joi.number().integer().required(),
    synopsis: Joi.string().max(500)
});

module.exports = Movie;
