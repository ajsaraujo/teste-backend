const { Schema, model } = require('mongoose');

const RevokedTokenSchema = Schema({
    value: {
        type: String,
        required: true
    }
});

const RevokedToken = model('RevokedToken', RevokedTokenSchema);

module.exports = RevokedToken;
