// Não podemos usar arrow function pois perderíamos a referência ao this.
/* eslint-disable func-names */

const { Schema, model } = require('mongoose');
const bcrypt = require('bcrypt');
const Joi = require('joi');
const PasswordUtils = require('../utils/PasswordUtils');

const UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true
    },

    isActive: {
        type: Boolean,
        default: true,
    },

    role: {
        type: String,
        default: 'User'
    }
}, {
    timestamps: true,
    discriminatorKey: 'role'
});

const encrypter = new PasswordUtils(bcrypt);

UserSchema.pre('save', async function (next) {
    // Jogamos pra lower case pra evitar coisas estranhas na busca.
    this.email = this.email.toLowerCase();

    // Só quem pode escrever em isActive é o método de exclusão lógica.
    this.isActive = true;

    this.password = await encrypter.encrypt(this.password);

    next();
});

UserSchema.statics.findOneActive = async function (query) {
    const user = await this.findOne(query);

    if (user && !user.isActive) {
        return null;
    }

    return user;
};

const User = model('User', UserSchema);

const nameRegex = new RegExp(/^[A-Za-z áéíóúÁÉÍÓÚãõÃÕâêôÂÊÔ]+$/);
const nameRules = Joi.string().required().pattern(nameRegex).max(100);
const emailRules = Joi.string().email().required();

User.joiValidator = Joi.object({
    email: emailRules,
    name: nameRules,
    password: Joi.string().required().min(6).max(100)
});

User.authValidator = Joi.object({
    email: emailRules,
    password: Joi.string().required()
});

module.exports = User;
