const { Schema } = require('mongoose');
const User = require('./User');

const AdminSchema = new Schema({});

const Admin = User.discriminator('Admin', AdminSchema);

module.exports = Admin;
