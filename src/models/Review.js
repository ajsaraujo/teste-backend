const { Schema, model } = require('mongoose');
const Joi = require('joi');

const ReviewSchema = Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

    movieId: {
        type: Schema.Types.ObjectId,
        ref: 'Movie',
        required: true
    },

    rating: {
        type: Number,
        required: true
    },

    comment: {
        type: String,
        required: false
    }
});

const Review = model('Review', ReviewSchema);

// Não recebemos o userId pq ele vem no token
Review.joiValidator = Joi.object({
    movieId: Joi.string().required(),
    comment: Joi.string().max(2500),
    rating: Joi.number().integer().min(0).max(4)
        .required(),
});

module.exports = Review;
