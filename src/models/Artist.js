const Joi = require('joi');
const { model, Schema } = require('mongoose');

const ArtistSchema = Schema({
    name: {
        type: String,
        required: true
    },

    birthDate: {
        type: String,
        required: false
    },

    biography: {
        type: String,
        required: false
    }
});

const Artist = model('Artist', ArtistSchema);
const dateRegex = new RegExp(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/);

Artist.joiValidator = Joi.object({
    name: Joi.string().required(),
    birthDate: Joi.string().regex(dateRegex).error(new Error('You should provide a date with format dd/mm/yyyy')),
    biography: Joi.string().max(1000)
});

module.exports = Artist;
