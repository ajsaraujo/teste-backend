const { model, Schema } = require('mongoose');
const Joi = require('joi');
const Roles = require('../utils/ArtistRoleUtils');

const WorkSchema = Schema({
    movieId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Movie'
    },

    artistId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Artist'
    },

    role: {
        type: String,
        enum: [Roles.ACTOR, Roles.DIRECTOR]
    }
});

const Work = model('Work', WorkSchema);

Work.joiValidator = Joi.object({
    movieId: Joi.string().required(),
    artistId: Joi.string().required(),
    role: Joi.string.valid(Roles.ACTOR, Roles.DIRECTOR)
});

module.exports = Work;
