const express = require('express');
const mongoose = require('mongoose');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');

const { config } = require('dotenv');

const createApp = require('./app');
const createRouter = require('./router');

const limitRequests = require('./middlewares/limitRequests');

function addExitSignals(app, server, exitSignals) {
    exitSignals.forEach(signal => {
        process.on(signal, () => {
            server.close(err => {
                if (err) {
                    process.exit(1);
                }

                app.database.close(() => {
                    process.exit(0);
                });
            });
        });
    });
}

(async () => {
    config();

    const middlewares = [
        limitRequests.slightly,
        express.json(),
        compression(),
        cors(),
        helmet(),
        express.urlencoded({ extended: true })
    ];

    const router = await createRouter(express.Router());

    const app = createApp(express(), router, middlewares, mongoose);

    await app.database.connect();

    const server = app.listen(process.env.PORT, () => {
        console.log(`App is listening on port ${process.env.PORT}.`);
    });

    addExitSignals(app, server, ['SIGINT', 'SIGTERM', 'SIGQUIT']);
})();
