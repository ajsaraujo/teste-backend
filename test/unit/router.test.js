const { expect } = require('chai');
const { createSandbox } = require('sinon');
const createRouter = require('../../src/router');

describe('createRouter()', () => {
    let sandbox;
    let routerStub;
    
    beforeEach(() => {
        sandbox = createSandbox();
        routerStub = { get: sandbox.stub() };
    });

    it('should wait for GET requests on the root endpoint', () => {
        createRouter(routerStub);

        expect(routerStub.get.getCall(0).args[0]).to.equal('/');
    });

    it.skip('should return the Router', () => {
        const router = createRouter(routerStub);

        expect(router).to.equal(routerStub);
    });

    afterEach(() => {   
        sandbox.restore();
    });
});