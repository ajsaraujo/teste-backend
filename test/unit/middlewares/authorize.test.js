const { expect } = require('chai');
const { createSandbox } = require('sinon');
const TestUtils = require('../TestUtils');
const authorize = require('../../../src/middlewares/authorize');

describe('authorize', () => {
    let sandbox;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();

        req = TestUtils.mockReq();
        res = TestUtils.mockRes();
        next = TestUtils.mockNext(sandbox);
    });

    it('should return 403 if user role is not in the provided role list', () => {
        req.user = { role: 'User' };

        const { status, json } = authorize('Admin')(req, res, next);

        expect(status).to.equal(403);
        expect(json.message).to.equal('User does not have access to this resource.');
    });

    it('should return next if user has access', () => {
        req.user = { role: 'User' };

        authorize('Admin', 'User')(req, res, next);

        expect(next.calledOnce).to.be.true;
    });

    afterEach(() => {
        sandbox.restore();
    });
});