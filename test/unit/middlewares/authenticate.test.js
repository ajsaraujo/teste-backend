const { expect } = require('chai');
const { createSandbox } = require('sinon');
const authenticateMiddleware = require('../../../src/middlewares/authenticate');
const TestUtils = require('../TestUtils');

describe('authenticate', () => {
    let sandbox;
    let mockTokenUtils;
    let authenticate;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();
        mockTokenUtils = { verify: sandbox.stub(), isMalformed: sandbox.stub() };
        authenticate = authenticateMiddleware(mockTokenUtils);

        req = TestUtils.mockReq();
        res = TestUtils.mockRes();
        next = TestUtils.mockNext(sandbox);
    });

    it('should return 400 if token is malformed', () => {
        mockTokenUtils.isMalformed.returns(true);

        const { status, json } = authenticate(req, res, next);

        expect(status).to.equal(400);
        expect(json.message).to.equal('Malformed or missing token. You should provide a token with the following format: Bearer <token>');
    });

    it('should return 401 if token is not valid', () => {
        mockTokenUtils.isMalformed.returns(false);
        mockTokenUtils.verify.returns({ isValid: false });

        const { status, json } = authenticate(req, res, next);

        expect(status).to.equal(401);
        expect(json.message).to.equal('Invalid token. Refresh your token or authenticate again.');
    });

    it('should return next and write payload to req.user if everything is fine', () => {
        mockTokenUtils.isMalformed.returns(false);
        mockTokenUtils.verify.returns(
            {
                isValid: true,
                payload: {
                    id: '123456',
                    name: 'Allan',
                    email: 'allan@ioasys.com.br'
                }
            }
        );

        authenticate(req, res, next);

        expect(next.calledOnce).to.be.true;
        expect(req.user).to.eql({ id: '123456', name: 'Allan', email: 'allan@ioasys.com.br' });
    });

    afterEach(() => {
        sandbox.restore();
    });
});
