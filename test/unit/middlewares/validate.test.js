const { expect } = require('chai');
const { createSandbox } = require('sinon');
const validate = require('../../../src/middlewares/validate');
const TestUtils = require('../TestUtils');

describe('validate()', () => {
    let sandbox;
    let mockSchema;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();
        mockSchema = { validateAsync: sandbox.stub() };

        req = TestUtils.mockReq();
        res = TestUtils.mockRes();
        next = TestUtils.mockNext(sandbox);
    });

    it('should return 400 if bad data was sent', async () => {
        const error = new Error('Expected username to be defined');
        mockSchema.validateAsync.throws(error);

        const { status, json } = await validate(mockSchema)(req, res, next);

        expect(status).to.equal(400);
        expect(json.message).to.equal('Expected username to be defined');
    });

    it('should return next if everything looks fine', async () => {
        mockSchema.validateAsync.resolves('nice data');

        await validate(mockSchema)(req, res, next);

        expect(next.calledOnce).to.be.true;
    });

    afterEach(() => sandbox.restore() );
});