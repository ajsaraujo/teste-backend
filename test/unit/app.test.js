const { expect } = require('chai');
const { createSandbox } = require('sinon');
const createApp = require('../../src/app');
const Database = require('../../src/database');

describe('createApp()', () => {
    let sandbox;
    let expressStub;
    let routerStub;
    let mongooseStub;

    beforeEach(() => {
        sandbox = createSandbox();
        expressStub = { use: sandbox.stub() };
        routerStub = sandbox.stub();
        mongooseStub = { connect: sandbox.stub(), connection: { close: sandbox.stub() } };
    });

    it('should use every middleware', () => {
        const middlewares = [() => {}, () => {}];

        createApp(expressStub, routerStub, middlewares);

        middlewares.forEach(middleware => {
            expect(expressStub.use.calledWith(middleware)).to.be.true;
        });
    });

    it('should use the router on the /api endpoint', () => {
        createApp(expressStub, routerStub, []);

        expect(expressStub.use.getCall(0).args).to.eql(['/api', routerStub]);
    });

    it('should return the Express app', () => {
        const app = createApp(expressStub, routerStub, []);

        expect(app).to.equal(expressStub);
    });

    it('should create a database with the provided mongoose object', () => {
        const app = createApp(expressStub, routerStub, [], mongooseStub);

        expect(app.database instanceof Database).to.be.true;
        expect(app.database.mongoose).to.equal(mongooseStub);
    });

    afterEach(() => {
        sandbox.restore();
    });
});