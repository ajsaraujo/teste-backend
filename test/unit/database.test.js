const { expect } = require('chai');
const { createSandbox } = require('sinon');
const Database = require('../../src/database');

describe('Database', () => {
    let sandbox;
    let mockMongoose;
    let database;

    beforeEach(() => {
        sandbox = createSandbox();
        mockMongoose = { connect: sandbox.stub(), connection: { close: sandbox.stub() } };
        database = new Database(mockMongoose);
    });
    
    describe('constructor()', () => {
        it('should set the mongoose object', () => {
            expect(database.mongoose).to.equal(mockMongoose);
        });
    });

    describe('connect()', () => {
        it('should connect to the env uri', async () => {
            await database.connect();
            
            const uri = mockMongoose.connect.getCall(0).args[0];
            
            expect(uri).to.equal(process.env.DB_URI);
        });

        it('should pass correct options', async () => {
            await database.connect();

            const options = mockMongoose.connect.getCall(0).args[1];

            expect(options).to.eql({
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true,
                useFindAndModify: false,
            });
        });
    });

    describe('close()', () => {
        it('should close the connection', async () => {
            await database.close();

            expect(mockMongoose.connection.close.calledOnce).to.be.true;
        });
    });

    afterEach(() => {
        sandbox.restore();
    });
});