const { expect } = require('chai');
const { createSandbox } = require('sinon');
const TokenUtils = require('../../../src/utils/TokenUtils');

describe('TokenUtils', () => {
    let sandbox;
    let tokenUtils;
    let mockJwt;

    beforeEach(() => {
        sandbox = createSandbox();
        mockJwt = { sign: sandbox.stub(), verify: sandbox.stub() };
        tokenUtils = new TokenUtils(mockJwt);
    });

    describe('createAccessToken', () => {
        it('should call sign and return the token', () => {
            mockJwt.sign.returns('122445454343');

            const payload = { name: 'Allan', email: 'allan@ioasys.com.br' };

            const token = tokenUtils.createAccessToken(payload);

            const { args } = mockJwt.sign.getCall(0);

            expect(token).to.equal('Bearer 122445454343');
            expect(args).to.eql([
                payload,
                process.env.ACCESS_SECRET,
                { expiresIn: process.env.ACCESS_LIFE_SPAN }
            ]);
        });
    });

    describe('createRefreshToken', () => {
        it('should call sign and return the token', () => {
            mockJwt.sign.returns('122445454343');

            const payload = { name: 'Allan', email: 'allan@ioasys.com.br' };

            const token = tokenUtils.createRefreshToken(payload);

            const { args } = mockJwt.sign.getCall(0);

            expect(token).to.equal('Bearer 122445454343');
            expect(args).to.eql([
                payload,
                process.env.REFRESH_SECRET,
                { expiresIn: process.env.REFRESH_LIFE_SPAN }
            ]);
        });
    });

    describe('isMalformed', () => {
        it('should return true if token is a falsy value', () => {
            const falsyValues = ['', null, undefined];

            falsyValues.forEach(falsyValue => {
                expect(tokenUtils.isMalformed(falsyValue)).to.be.true;
            });
        });

        it('should return true if token is not in the Bearer <token> format', () => {
            const badValues = ['1213544', 'Bearer', 'Bierer 121325325'];

            badValues.forEach(badValue => {
                expect(tokenUtils.isMalformed(badValue)).to.be.true;
            });
        });

        it('should return false if the token looks good', () => {
            const token = 'Bearer 132546445686';

            expect(tokenUtils.isMalformed(token)).to.be.false;
        });
    });

    describe('verify', () => {
        it('should return isValid false if jwt.verify fails', () => {
            mockJwt.verify.throws(new Error('Bad token'));

            const { isValid } = tokenUtils.verify('Bearer 12345345', process.env.ACCESS_SECRET);

            expect(isValid).to.be.false;
        });

        it('should return isValid: true and payload if everything is fine', () => {
            mockJwt.verify.returns({ id: '123456', name: 'Allan', email: 'allan@ioasys.com.br' });

            const { isValid, payload } = tokenUtils.verify('Bearer 123456', process.env.ACCESS_SECRET);

            expect(isValid).to.be.true;
            expect(payload).to.eql({ id: '123456', name: 'Allan', email: 'allan@ioasys.com.br' });
        });
    });
});
