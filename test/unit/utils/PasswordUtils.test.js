const { expect } = require('chai');
const { createSandbox } = require('sinon');
const PasswordUtils = require('../../../src/utils/PasswordUtils');

describe('PasswordUtils', () => {
    let sandbox;
    let mockCrypto;
    let passwordUtils;

    beforeEach(() => {
        sandbox = createSandbox();
        mockCrypto = { hash: sandbox.stub(), compare: sandbox.stub() };
        passwordUtils = new PasswordUtils(mockCrypto);
    });

    describe('encrypt()', () => {
        it('should encrypt a string', async () => {
            mockCrypto.hash.returns('sfuo´defgbg~3t42[ji');
        
            const encryptedData = await passwordUtils.encrypt('minhasupersenha');

            expect(mockCrypto.hash.calledWith('minhasupersenha')).to.be.true;
            expect(encryptedData).not.to.equal('minhasupersenha');
        });
    });

    describe('match()', () => {
        it('should return false if plain text is falsy', async () => {
            const result = await passwordUtils.match('', 'gesPI{j[wti');
            
            expect(result).to.be.false;
        });

        it('should return false if encrypted text is falsy', async () => {
            const result = await passwordUtils.match('asenhadapessoa', '');
            
            expect(result).to.be.false;
        });

        it('should return true if it is a match', async () => {
            mockCrypto.compare.returns(true);
            
            const result = await passwordUtils.match('ola', 'ofsapkseo');

            expect(result).to.be.true;
        });

        it('should return false if it is not', async () => {
            mockCrypto.compare.returns(false);

            const result = await passwordUtils.match('ola', '\sdvnklvg\sdkngdskn');
            
            expect(result).to.be.false;
        });
    });

    afterEach(() => {
        sandbox.restore();
    });
});