const { expect } = require('chai');
const { createSandbox } = require('sinon');
const TokenController = require('../../../src/controllers/TokenController');
const TestUtils = require('../TestUtils');

describe('TokenController', () => {
    let sandbox;
    let mockRevokedToken;
    let mockTokenUtils;
    let tokenController;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();

        mockTokenUtils = {
            createAccessToken: sandbox.stub(),
            verify: sandbox.stub(),
            isMalformed: sandbox.stub()
        };

        mockRevokedToken = { findOne: sandbox.stub(), create: sandbox.stub() };

        tokenController = new TokenController(mockRevokedToken, mockTokenUtils);

        req = TestUtils.mockReq();
        res = TestUtils.mockRes();
        next = TestUtils.mockNext(sandbox);

        req.headers['x-refresh-token'] = 'Bearer bliblbibof';
    });

    describe('refresh()', () => {
        it('should return 400 if refresh token is malformed', async () => {
            mockTokenUtils.isMalformed.returns(true);

            const { status, json } = await tokenController.refresh(req, res, next);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Malformed token. You should provide a token with the following format: Bearer <token>.');
        });

        it('should return 401 if refresh token is not valid', async () => {
            mockTokenUtils.isMalformed.returns(false);
            mockTokenUtils.verify.returns({ isValid: false });

            const { status, json } = await tokenController.refresh(req, res, next);

            expect(status).to.equal(401);
            expect(json.message).to.equal('Invalid token. Please authenticate again.');
        });

        it('should return 401 if refresh token was revoked', async () => {
            mockTokenUtils.isMalformed.returns(false);
            mockTokenUtils.verify.returns({ isValid: true });
            mockRevokedToken.findOne.resolves({});

            const { status, json } = await tokenController.refresh(req, res, next);

            expect(status).to.equal(401);
            expect(json.message).to.equal('Invalid token. Please authenticate again.');
        });

        it('should return 200 and a new access token if everything looks fine', async () => {
            mockTokenUtils.isMalformed.returns(false);
            mockRevokedToken.findOne.resolves(null);
            mockTokenUtils.verify.returns(
                {
                    isValid: true,
                    payload: {
                        name: 'Allan',
                        email: 'allan@ioasys.com.br',
                        id: '123456'
                    }
                }
            );
            mockTokenUtils.createAccessToken.returns('Bearer my_access_token');

            const { status, json } = await tokenController.refresh(req, res, next);

            expect(status).to.equal(200);
            expect(json['x-access-token']).to.equal('Bearer my_access_token');
            expect(mockTokenUtils.createAccessToken.getCall(0).args[0]).to.eql({
                name: 'Allan',
                email: 'allan@ioasys.com.br',
                id: '123456'
            });
        });
    });

    describe('revoke', () => {
        it('should return 400 if token is malformed', async () => {
            mockTokenUtils.isMalformed.returns(true);

            const { status, json } = await tokenController.revoke(req, res, next);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Malformed token. You should provide a token with the following format: Bearer <token>.');
        });

        it('should return 200 if token is not valid', async () => {
            mockTokenUtils.isMalformed.returns(false);
            mockTokenUtils.verify.returns({ isValid: false });
            mockRevokedToken.findOne.returns(null);

            const { status, json } = await tokenController.revoke(req, res, next);

            expect(status).to.equal(200);
            expect(json.message).to.equal('Token revoked successfully.');
            expect(mockRevokedToken.create.calledWith({ value: req.headers['x-refresh-token'].split(' ')[1] })).to.be.false;
        });

        it('should return 200 if token was already revoked', async () => {
            mockTokenUtils.isMalformed.returns(false);
            mockTokenUtils.verify.returns({ isValid: true });
            mockRevokedToken.findOne.returns({});

            const { status, json } = await tokenController.revoke(req, res, next);

            expect(status).to.equal(200);
            expect(json.message).to.equal('Token revoked successfully.');
            expect(mockRevokedToken.create.calledWith({ value: req.headers['x-refresh-token'].split(' ')[1] })).to.be.false;
        });

        it('should return 200 if token was successfully revoked', async () => {
            mockTokenUtils.isMalformed.returns(false);
            mockTokenUtils.verify.returns({ isValid: true });
            mockRevokedToken.findOne.resolves(null);

            const { status, json } = await tokenController.revoke(req, res, next);

            expect(status).to.equal(200);
            expect(json.message).to.equal('Token revoked successfully.');
            expect(mockRevokedToken.create.getCall(0).args[0].value).to.equal(req.headers['x-refresh-token'].split(' ')[1]);
        });
    });

    describe('isRevoked()', () => {
        it('should return true if a token is found', async () => {
            mockRevokedToken.findOne.resolves({ value: 'bliblibloblbo' });

            const isRevoked = await tokenController.isRevoked('Bearer blabla');

            expect(Boolean(isRevoked)).to.be.true;
        });

        it('should return false otherwise', async () => {
            mockRevokedToken.findOne.resolves(null);

            const isRevoked = await tokenController.isRevoked('Bearer blabla');

            expect(Boolean(isRevoked)).to.be.false;
        });
    });

    afterEach(() => sandbox.restore());
});
