const { expect } = require('chai');
const { createSandbox } = require('sinon');
const TestUtils = require('../TestUtils');
const ReviewController = require('../../../src/controllers/ReviewController');

describe('ReviewController', () => {
    let sandbox;
    let mockMovie;
    let mockReview;
    let mockUser;
    let reviewController;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();

        mockReview = { create: sandbox.stub() };
        mockMovie = { findById: sandbox.stub() };
        mockUser = { findOneActive: sandbox.stub() };

        reviewController = new ReviewController(mockReview, mockMovie, mockUser);

        req = TestUtils.mockReq();
        res = TestUtils.mockRes();
        next = TestUtils.mockNext(sandbox);

        req.user = {
            id: '123456'
        };

        req.body = {
            movieId: '1234567',
            rating: 0,
            comment: 'This movie SUCKS >:('
        };
    });

    describe('create()', () => {
        it('should return 404 if movie was not found', async () => {
            mockMovie.findById.resolves(null);
            mockUser.findOneActive.resolves({});

            const { status, json } = await reviewController.create(req, res, next);

            expect(status).to.equal(404);
            expect(json.message).to.equal('Movie not found.');
        });

        it('should return 404 if user was not found', async () => {
            mockUser.findOneActive.resolves(null);
            mockMovie.findById.resolves({});

            const { status, json } = await reviewController.create(req, res, next);

            expect(status).to.equal(404);
            expect(json.message).to.equal('User not found.');
        });

        it('should create a review', async () => {
            mockMovie.findById.resolves({});
            mockUser.findOneActive.resolves({});
            mockReview.create.callsFake(data => data);

            const { status, json } = await reviewController.create(req, res, next);

            const expectedResponse = req.body;
            expectedResponse.userId = req.user.id;

            expect(status).to.equal(201);
            expect(json).to.eql(expectedResponse);
        });
    });

    afterEach(() => {
        sandbox.restore();
    });
});