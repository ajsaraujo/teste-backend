const { expect } = require('chai');
const { createSandbox } = require('sinon');
const TestUtils = require('../TestUtils');
const ArtistController = require('../../../src/controllers/ArtistController');

describe('ArtistController', () => {
    let sandbox;
    let mockArtist;
    let artistController;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();

        mockArtist = { create: sandbox.stub() };

        artistController = new ArtistController(mockArtist);

        req = TestUtils.mockReq();
        res = TestUtils.mockRes();
        next = TestUtils.mockNext(sandbox);
    });

    describe('create', () => {
        it('should create an artist', async () => {
            mockArtist.create.resolves({
                name: 'Brad Pitt',
                birthDate: '18/12/1963',
                biography: 'Brad is a very good looking guy who has once dated Angelina Jolie. He won the Oscar for best supporting role in Once Upon a Time... in Hollywood'
            });

            const { status, json } = await artistController.create(req, res, next);

            expect(status).to.equal(201);
            expect(json).to.eql({
                name: 'Brad Pitt',
                birthDate: '18/12/1963',
                biography: 'Brad is a very good looking guy who has once dated Angelina Jolie. He won the Oscar for best supporting role in Once Upon a Time... in Hollywood'
            });
        });
    });

    afterEach(() => {
        sandbox.restore();
    });
});