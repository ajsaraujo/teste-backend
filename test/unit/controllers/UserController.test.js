const { expect } = require('chai');
const { createSandbox, mock } = require('sinon');
const TestUtils = require('../TestUtils');
const UserController = require('../../../src/controllers/UserController');

describe('UserController', () => {
    let sandbox;
    let mockUser;
    let mockMatcher;
    let mockTokenUtils;
    let userController;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();

        mockUser = {
            findOne: sandbox.stub(),
            findOneActive: sandbox.stub(),
            create: sandbox.stub(),
            findByIdAndUpdate: sandbox.stub(),
        };

        mockMatcher = { match: sandbox.stub() };
        mockTokenUtils = { createAccessToken: sandbox.stub(), createRefreshToken: sandbox.stub() };

        userController = new UserController(mockUser, mockMatcher, mockTokenUtils);

        req = TestUtils.mockReq();
        res = TestUtils.mockRes();
        next = TestUtils.mockNext(sandbox);
    });

    describe('register()', () => {
        it('should return 400 if email is already in use', async () => {
            mockUser.findOne.resolves({ email: 'allan@ioasys.com.br' });

            req.body.email = 'allan@ioasys.com.br';

            const { status, json } = await userController.register(req, res, next);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Email allan@ioasys.com.br is already in use.');
        });

        it('should create a new user and return 201', async () => {
            mockUser.findOne.resolves(null);
            mockUser.create.resolves({
                name: 'Allan Juan',
                email: 'allan@ioasys.com.br',
                password: 'FEGW{IOE[iEIJgjdsg',
                isActive: true
            });

            const { status, json } = await userController.register(req, res, next);

            expect(status).to.equal(201);
            expect(json).to.eql({
                name: 'Allan Juan', email: 'allan@ioasys.com.br', password: undefined, isActive: undefined
            });
            expect(mockUser.create.calledWith(req.body)).to.be.true;
        });
    });

    describe('auth', () => {
        beforeEach(() => {
            req.body = {
                email: 'allan@ioasys.com.br',
                password: 'ih oasis'
            };
        });

        it('should return 400 if user is not found', async () => {
            mockUser.findOneActive.returns(null);

            const { status, json } = await userController.auth(req, res, next);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Invalid email and/or password.');
        });

        it('should return 400 if password is not correct', async () => {
            mockUser.findOneActive.returns({
                name: 'Allan Juan',
                email: 'allan@ioasys.com.br',
                password: 'greiwuh23úhr´sduhfsdu'
            });

            mockMatcher.match.resolves(false);

            const { status, json } = await userController.auth(req, res, next);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Invalid email and/or password.');
        });

        it('should return 200 if password is correct', async () => {
            mockUser.findOneActive.returns({
                name: 'Allan Juan',
                email: 'allan@ioasys.com.br',
                password: 'greiwuh23úhr´sduhfsdu',
                isActive: true,
            });

            mockMatcher.match.resolves(true);
            mockTokenUtils.createAccessToken.returns('Bearer access_token');
            mockTokenUtils.createRefreshToken.returns('Bearer refresh_token');

            const { status, json } = await userController.auth(req, res, next);

            expect(status).to.equal(200);
            expect(json.user).to.eql({
                name: 'Allan Juan',
                email: 'allan@ioasys.com.br',
                password: undefined,
                isActive: undefined
            });

            expect(json['x-access-token']).to.equal('Bearer access_token');
            expect(json['x-refresh-token']).to.equal('Bearer refresh_token');
        });

        it('should return 400 if credentials are fine but account is not active', async () => {
            mockUser.findOneActive.returns({
                name: 'Allan Juan',
                email: 'allan@ioasys.com.br',
                password: 'abbubbububleee',
                isActive: false
            });

            mockMatcher.match.resolves(true);

            const { status, json } = await userController.auth(req, res, next);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Invalid email and/or password.');
        });
    });

    describe('update', () => {
        beforeEach(() => {
            req.user = {
                id: '123456',
                name: 'Ben Affleck',
                email: 'benaff@hollywoodmail.com',
                password: 'pipipopo',
            };
        });

        it('should return 404 if user is not found', async () => {
            mockUser.findOneActive.resolves(null);

            const { status, json } = await userController.update(req, res, next);

            expect(status).to.equal(404);
            expect(json.message).to.equal('User not found.');
        });

        it('should return 400 if user tries to update his email to one that is already taken', async () => {
            mockUser.findOneActive.resolves({});

            req.body = {
                id: '123456',
                name: 'Ben Affleck',
                email: 'benaffleck@hollywoodmail.com',
                password: 'pipipopo'
            };

            const { status, json } = await userController.update(req, res, next);

            expect(status).to.equal(400);
            expect(json.message).to.equal(`Email ${req.body.email} is already taken.`);
        });

        it('should return 200 and correctly update user data', async () => {
            const mockUserInstance = { save: sandbox.stub() };

            mockUser.findOneActive.resolves(mockUserInstance);

            req.body = {
                id: '123456',
                name: 'Ben Affleck',
                email: 'benaff@hollywoodmail.com',
                password: 'pipipopo'
            };

            const { status, json } = await userController.update(req, res, next);

            expect(status).to.equal(200);
            expect(json.id).to.equal('123456');
            expect(json.name).to.equal('Ben Affleck');
            expect(json.email).to.equal('benaff@hollywoodmail.com');
            expect(json.password).to.be.undefined;
            expect(json.isActive).to.be.undefined;
            expect(mockUserInstance.save.calledOnce).to.be.true;
        });
    });

    describe('delete', () => {
        beforeEach(() => {
            req.user = { id: '123456767' };
        });

        it('should return 404 if user is not found', async () => {
            mockUser.findOneActive.resolves(null);

            const { status, json } = await userController.delete(req, res, next);

            expect(status).to.equal(404);
            expect(json.message).to.equal('User not found.');
        });

        it('should return 200 and set isActive to false', async () => {
            mockUser.findOneActive.resolves({});

            const { status, json } = await userController.delete(req, res, next);

            expect(status).to.equal(200);
            expect(json.message).to.equal('Account deleted.');
            expect(mockUser.findByIdAndUpdate.calledOnce).to.be.true;
        });
    });

    describe('promote', () => {
        beforeEach(() => {
            req.params.id = '1234567';
        });

        it('should return 400 if no id is provided', async () => {
            req.params.id = undefined;

            const { status, json } = await userController.promote(req, res, next);

            expect(status).to.equal(400);
            expect(json.message).to.equal('You should provide an user id at the request parameters.');
        });

        it('should return 404 if user is not found', async () => {
            mockUser.findOneActive.returns(null);

            const { status, json } = await userController.promote(req, res, next);

            expect(status).to.equal(404);
            expect(json.message).to.equal('User not found.');
        });

        it('should return 200, change user role to admin and save', async () => {
            const myUser = { save: sandbox.stub() };
            mockUser.findOneActive.returns(myUser);

            const { status, json } = await userController.promote(req, res, next);

            expect(status).to.equal(200);
            expect(json.message).to.equal('User was promoted.');
            expect(myUser.role).to.equal('Admin');
            expect(myUser.save.calledOnce).to.be.true;
        });
    });

    afterEach(() => {
        sandbox.restore();
    });
});
