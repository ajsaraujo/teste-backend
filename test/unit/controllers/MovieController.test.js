const { expect } = require('chai');
const { createSandbox } = require('sinon');
const MovieController = require('../../../src/controllers/MovieController');
const TestUtils = require('../TestUtils');

describe('MovieController', () => {
    let sandbox;
    let mockMovie;
    let mockReview;
    let movieController;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();
        mockMovie = { create: sandbox.stub(), findById: sandbox.stub() };
        mockReview = { find: sandbox.stub() };

        movieController = new MovieController(mockMovie, mockReview);

        req = TestUtils.mockReq();
        res = TestUtils.mockRes();
        next = TestUtils.mockNext(sandbox);
    });

    describe('create', () => {
        it('should create a new movie', async () => {
            req.body = {
                title: 'An American Werewolf in London',
                year: 1981,
                length: 97,
                synopsis: 'Two American tourists in England are attacked by a werewolf that none of the locals will admit exists',
            };

            mockMovie.create.callsFake(data => data);

            const { status, json } = await movieController.create(req, res, next);

            expect(status).to.equal(201);
            expect(json).to.eql(req.body);
        });
    });

    describe('get', () => {
        it('should return 404 if movie is not found', async () => {
            mockMovie.findById.resolves(null);

            const { status, json } = await movieController.get(req, res, next);

            expect(status).to.equal(404);
            expect(json.message).to.equal('Movie not found.');
        });

        it('should return 200 and movie + all reviews and average', async () => {
            const movieInstance = {
                title: 'What We do In The Shadows',
                year: 2014,
                synopsis: 'Vampire housemates try to cope with the complexities of modern life and show a newly turned hipster some of the perks of being undead.',
                length: 86,
                toObject: () => movieInstance
            };

            const reviewArray = [
                {
                    userId: '123456',
                    rating: 4,
                    comment: 'kkkkk velho esse filme é uma resenha!!',
                    movieId: {
                        toString: () => req.params.id
                    }
                },
                {
                    userId: '654321',
                    rating: 2,
                    comment: 'assisti a pulso quase saio do cinema',
                    movieId: {
                        toString: () => req.params.id
                    }
                }
            ];

            mockMovie.findById.resolves(movieInstance);
            mockReview.find.resolves(reviewArray);

            const { status, json } = await movieController.get(req, res, next);

            const expectedResult = movieInstance;
            expectedResult.reviews = reviewArray;

            expect(status).to.equal(200);
            expect(json).to.eql(expectedResult);
            expect(json.averageRating).to.equal(3);
        });
    });

    afterEach(() => {
        sandbox.stub();
    });
});
