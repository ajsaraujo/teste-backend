function mockReq() {
    return { body: {}, params: {}, headers: {} };
}

function mockRes() {
    const Response = class Response {
        status(statusCode) {
            this.status = statusCode;
            return this;
        }

        json(data) {
            this.json = data;
            return this;
        }
    };

    return new Response();
}

function mockNext(sandbox) {
    return sandbox.stub();
}

module.exports = { mockReq, mockRes, mockNext };