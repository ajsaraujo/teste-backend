Seja bem vindo! Aqui vou trazer algumas informações relevantes e falar sobre o meu processo de desenvolvimento.

### Links Úteis

- [Instância do App no Heroku](https://floating-journey-76590.herokuapp.com/api)
- [Coleção Postman](https://www.getpostman.com/collections/39de0d9438aa5af66613)

### Instalação

```sh
# Clone o repositório
$ git clone https://bitbucket.org/ajsaraujo/teste-backend/src/master/

cd teste-backend/

# Instale as dependências
$ npm i
```

Uma vez feito isso, crie um arquivo chamado `.env` em `teste-backend/`. Copie o conteúdo de `.env.example`
e cole nele. Modifique os valores das variáveis conforme necessário.

- ▶️ Rode `npm start` para iniciar a aplicação
- 🧪 Rode `npm test` para executar os testes

A coleção do Postman usa uma **variável prefix**. Modifique-a de acordo com a porta que você está usando pra hospedar o app. Aqui na minha máquina, prefix ficou assim: `localhost:8080/api`. Lembre-se que se não quiser rodar na sua máquina, o app está hospedado no Heroku, basta você utilizar a variável **heroku** ao invés. Note que você pode experimentar um delay considerável na resposta da primeira requisição, já que o Heroku encerra o app caso ele fique inativo por mais de 30 minutos, fazendo a inicialização apenas com a chegada de novas requisições. 

Evite utilizar tokens de forma intercambiável ao fazer requisições no app do Heroku e na instância local - eles terão sido assinados com chaves diferentes e portanto não funcionarão como esperado.

### Usuário Admin

A API não expõe nenhuma forma de criar um usuário admin "do nada", apenas uma rota em que um usuário admin pode promover um usuário normal a admin. Com isso, para criar um usuário admin em sua instância do app, você deve cadastrá-lo através da rota `register/` (ou através do dashboard do Mongo, se preferir), e depois editar sua role para Admin no dashboard. Se você está testando contra o heroku, basta se autenticar em `auth/` com as credenciais a seguir:

```json
{
    "email": "user@ioasys.com.br",
    "password": "notadmin"
}
```

### Funcionalidades
- Usuário
    - ☑️ Cadastro
    - ☑️ Autenticação
    - ☑️ Edição
    - ☑️ Exclusão Lógica
    - ☑️ Promoção para ADM
- Filmes
    - ☑️ Cadastro
    - ☑️ Review
    - ☑️ Detalhes
    - Listagem
        - 🚧 Por diretor
        - 🚧 Por nome
        - 🚧 Por gênero
        - 🚧 Por atores

## Considerações

### Por que MongoDB?

Gosto mais da API do Mongoose do que a do Sequelize. Além disso, no momento minha
máquina só tem instalado o MongoDB, não queria perder tempo configurando e fazendo
troubleshooting de um novo banco de dados.

### Diagrama de Banco de Dados

Uma das primeiras coisas que fiz foi o diagrama do BD. Na implementação houveram algumas diferenças sutis, mas a grosso modo, o app segue esse design. A maior delas é que eu iria utilizar `Person` como classe pai de ambos `User` e `Artist`, isso por eles compartilharem algumas propriedades em comum. Mas depois eu me dei conta que isso acarretaria em os atores, diretores e usuários serem todos armazenados na mesma coleção, o que não seria legal, então optei por separar. 

Apesar de eu ter optado por um banco NoSql, modelei de forma bem relacional, usando tabelas associativas para lidar com as relações N para N. No entanto, no mundo real essa aplicação teria bem mais operações de leitura do que de escrita (no que se refere aos filmes, artistas, etc), então _talvez_ fizesse mais sentido manter esses dados aninhados, com fácil acesso em seus respectivos documentos. Mas essa é uma decisão que requer uma análise mais calma, na dúvida eu fui no bom e velho relacionamento.

![](docs/diagrama-db.png)

### Por que usou CommonJS (require()) e não Import/Export do ES6? 

Eu prefiro a sintaxe do ES6. Porém, eu me deparei com alguns problemas um tanto quanto esotéricos na configuração do Babel. Resolvi que não valeria a pena me demorar muito nisso, e optei pelo CommonJS.

### Fluxo de Autenticação

Busquei seguir as diretrizes do OAuth2 pra implementar o fluxo de autenticação, utilizando um par de tokens. Implementei o seguinte fluxo de autenticação:

1. Usuário autentica com login e senha na rota `/auth`. 
2. A API retornará um `x-access-token` de vida curta e um `x-refresh-token` de vida longa.
3. O usuário utilizará o `x-access-token` onde for necessária a autenticação.
4. Eventualmente, o `x-access-token` expirará, então bastará mandar o `x-refresh-token` para a rota `/refresh`, que um novo `x-access-token` será gerado.
5. Se por ventura o usuário mudar de senha, desejar dar log out, ou sofrer um ataque, o seu refresh token poderá ser inutilizado através da rota `/revoke`. 

### Tratamento de Erros

Para a saúde do App, a documentação do Express recomenda que todos os erros sejam capturados. Daí utilizei try/catch em todas as funções de controle, o que fica um pouco deselegante. Idealmente estaria utilizando alguma função wrapper que dispensasse esse uso exaustivo de try catch, mas dei prioridade a outras coisas e acabei não implementando isso.