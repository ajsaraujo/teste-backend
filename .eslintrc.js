module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true,
        mocha: true
    },
    extends: [
        'airbnb-base',
    ],
    parserOptions: {
        ecmaVersion: 12,
    },
    rules: {
        indent: ['error', 4],
        'arrow-parens': 'off',
        'comma-dangle': 'off',
        'linebreak-style': 'off'
    },
    overrides: [
        {
            files: ['test/**/*.test.js'],
            rules: {
                'no-unused-expressions': 'off'
            }
        }
    ]
};
